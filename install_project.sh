#!/bin/bash

. ./common.sh
. ./.env

if [[ -z "$DIR_PROJECT" ]]; then
    echo "[WARNING] Project directory not set."
    exit 0
fi

PATH_DIR_PROJECT=$DIR_ROOT_PROJECT/../src/$DIR_PROJECT

if [[ -d "$PATH_DIR_PROJECT" ]]; then
    echo "[INFO] The directory: $PATH_DIR_PROJECT exists."
else
    echo "[INFO] Create a directory: $PATH_DIR_PROJECT."
    sudo --user=$1 --group=$2 mkdir --mode=777 $PATH_DIR_PROJECT
    echo "[INFO] The result of executing the mkdir command:"
    ls -l $DIR_ROOT_PROJECT/../src/
fi
