#!/bin/bash

. ./common.env
. ./common.sh
. ./.env

PATH_USER_SQL=$DIR_ROOT_PROJECT/core/db/user.sql

if [[ $ENABLED_MYSQL_CREATE_USERS -eq 1 ]]; then
    echo "[INFO] Updating users in the database."
    $DOCKER_CMD exec -i $DB_CONTAINER_NAME mysql --user=$MYSQL_USER_DEFAULT --password=$MYSQL_ROOT_PASSWORD_DEFAULT < $PATH_USER_SQL
else
    echo "[WARNING] Updating users in the database has been disabled."
fi

if [[ $ENABLED_MYSQL_CREATE_DUMP_DB -eq 1 && -f "$PATH_FULL_FILE_DUMP_DB" ]]; then
    echo "[INFO] Database restoration."
    $DOCKER_CMD exec -i $DB_CONTAINER_NAME mysql --user=$MYSQL_USER_DEFAULT --password=$MYSQL_ROOT_PASSWORD_DEFAULT < $PATH_FULL_FILE_DUMP_DB
else
    echo "[WARNING] File $PATH_FULL_FILE_DUMP_DB does not exist or database restoration has been disabled."
fi
