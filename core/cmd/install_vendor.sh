#!/bin/bash

cd /var/www/$1

echo "Clean vendor ..."
rm -rf /var/www/$1/vendor/*

echo "Composer install ..."
composer install --dev

echo "Set permitions for file ..."
find /var/www/$1/vendor/* -type d -exec chmod 777 {} +
find /var/www/$1/vendor/* -type f -exec chmod 666 {} +
